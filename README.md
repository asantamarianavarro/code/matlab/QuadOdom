# Matlab Quadrotor Odometry

Visual-Inertial Odometry for Quadrotors using EKF and ESKF as filtering engines.

Currently supported sensors: IMU (filter propagation), optical flow smart camera (filter correction) and downward range sensor (filter correction).

## If you use this code, please cite:

  * A. Santamaria-Navarro, J. Solà and J. Andrade-Cetto. High-frequency MAV state estimation using low-cost inertial and optical flow measurement units, 2015 IEEE/RSJ International Conference on Intelligent Robots and Systems, 2015, Hamburg, pp. 1864-1871.
  * A. Santamaria-Navarro, G. Loianno, J. Solà, V. Kumar and J. Andrade-Cetto. Autonomous navigation of micro aerial vehicles using high-rate and low-cost sensors. 2016. Submitted to Autonomous Robots.

## Example of usage

- This code is prepared to work with the [MATLAB quadrotor simulation](https://gitlab.com/asantamarianavarro/code/matlab/QuadSim)
- An example of usage is included in `/data/simu` (obtained using QuadSim). To run it execute: `main.m`
- An example of plotting results is also provided (in results folder). To run it execute: `main_plot.m`

## Support material and multimedia

Please, visit: [**asantamaria's web page**](http://www.angelsantamaria.eu)