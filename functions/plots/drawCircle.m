% 
% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Draw a simple circle
%
%   [points] = drawCircle(coord,offset)
%
%   - Inputs:
%       - coord:    Coordinates of the initial point of the perimeter. 
%       - offset:   Angle offset in the perimeter to start the line.
%
%   - Outputs:
%       - points:   Points of the circle.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [points] = drawCircle(coord,offset)

t = 0:pi/8:2*pi;
r=0.2;
circle=zeros(3,size(t,2));
for j = 1:length(t)
    circle(:,j) = [r*sin(t(j)+offset);r*cos(t(j)+offset);0];
end

points = repmat(coord,1,size(circle,2))+circle;

end