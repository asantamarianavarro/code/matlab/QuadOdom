% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Draw outdoor arena
% 
%   [] = draw_arena(hax,hfloor)
% 
%   This script plots a scenario example.
% 
%   - Inputs:
%       - hax:      Axis handler where the drawings are placed.
%       - hfloor:   Initial floor handler.
% 
%   - Outputs:
%       - harena:   Plot Handlers.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [harena] = draw_arena(hax,hfloor)

hcyl1(1,:) = drawCylinder(hax,[2,2,0],[2,2,2],0.15,20,'r',1,0,1); %RED cylinder
hcyl2(1,:) = drawCylinder(hax,[2,-2,0],[2,-2,2],0.15,20,'r',1,0,1); %RED cylinder
hcyl3(1,:) = drawCylinder(hax,[-2,2,0],[-2,2,2],0.15,20,'r',1,0,1); %RED cylinder
hcyl4(1,:) = drawCylinder(hax,[-2,-2,0],[-2,-2,2],0.15,20,'r',1,0,1); %RED cylinder

hwall(1) = drawRectangle(hax,[-4;-4;0],16,-0.1,4,[0.9 0.9 0.9],0,1); %wall
hwall(2) = drawRectangle(hax,[12;-4.1;0],0.1,8.2,4,[0.9 0.9 0.9],0,1); %wall
hwall(3) = drawRectangle(hax,[-4;4;0],16,0.1,4,[0.9 0.9 0.9],0,1); %wall

%Landing area at 10m
hland = drawRectangle(hax,[9.75;-0.25;0],0.5,0.5,0,[0 0 0],1,0); %wall

axis([-5 5 -4 4 0 5]);
updateFloor(hax,hfloor);

harena = [hcyl1 hcyl2 hcyl3 hcyl4 hland hwall];

return