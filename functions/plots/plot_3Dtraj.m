% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Plot 3D Trajectories
% 
%   [] = plot_3Dtraj(fig,params,robots)
% 
%   This script plots the 3D trajectories for all robots.
% 
%   - Inputs:
%       - fig:      Number for figure handler.
%       - params:   Plot parameters.
%       - robots:   Robot structure. See: 'newrobot.m'
% 
%   - Outputs:
%       - plot_hdl: Plot Handlers.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [plot_hdl] = plot_3Dtraj(fig,params,robots)

% Figure Handlers
plot_hdl.hfig = figure(fig);
plot_hdl.hax = axes('Parent',plot_hdl.hfig);
axis equal
xlabel('x (m)'); ylabel('y (m)'); zlabel('z (m)');
set_figure_params(plot_hdl.hfig,'3D Trajectory',30,[300, 300, 1000, 700]);
az = params.traj.view.az;
el = params.traj.view.el;
view(plot_hdl.hax,az,el);
axis(plot_hdl.hax,params.traj.scene_size);
camzoom(1.0);
hold on

% Main handlers: Draw functions    
plot_hdl.floor = drawFloor(plot_hdl.hax); %Floor reference
switch params.traj.arena
    case 'arena'
        draw_arena(plot_hdl.hax,plot_hdl.floor);
    case 'empty'            
end
plot_hdl.frames.world = drawFrame(plot_hdl.hax,params.traj.scene_center,params.rob.frame_length); %World frame
plot_hdl.robot.quad_gen = drawRobot(plot_hdl.hax,params.traj.scene_center,params.rob.size_m2m,[0 0 0],params.rob.frame_length,'off'); %Generic Robot 
for robn=1:size(robots,2)
    if robn == 1
        linestyle = '-';
    else 
        linestyle = '-.';
    end
    plot_hdl.robot.quad(robn).hndl = drawRobot(plot_hdl.hax,robots(robn).state(1:3,1),robots(robn).size_m2m,robots(robn).color,robots(robn).frame_len,'on'); %Robot
    plot_hdl.robot.quad(robn).odom = drawLine(plot_hdl.hax,robots(robn).state(1:3,1),robots(robn).color,linestyle); %Odometry line
end

% Update all robots for all time steps
for tstep=2:size(robots(1).t,2)
    if mod(tstep,params.traj.draw_every)==0
        for robn=1:size(robots,2)
            updateRobot(robots(robn).state(:,tstep),plot_hdl.robot.quad_gen,plot_hdl.robot.quad(robn).hndl);
            updateLine(plot_hdl.robot.quad(robn).odom,robots(robn).state(1:3,tstep));
            updateAxis(plot_hdl.hax,robots(robn).state(:,tstep));
        end
        updateFloor(plot_hdl.hax,plot_hdl.floor);
        drawnow       
    end
end

end