% 
% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Draws a rectangle in specified parent handler
%
%   [hRec] = drawRectangle(parent,pos,width,deep,height,color,lines,alpha)
%
%   Set parameters to figure handle.
%   
%   Inputs:
%       - parent:   Parent handler where to draw the rectangle.   
%       - pos:      Position in the parent for the center of the rectangle.
%       - width:    Rectangle width.
%       - deep:     Rectangle deep.
%       - height:   Rectangle height.
%       - color:    Color.
%       - lines:    Show (1) or not (0) the rectangle edges.
%       - alpha:    Surface alpha
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [hRec] = drawRectangle(parent,pos,width,deep,height,color,lines,alpha)
 
vert = [pos(1)          pos(2)          pos(3); 
        pos(1)          pos(2)+deep     pos(3); 
        pos(1)+width    pos(2)+deep     pos(3); 
        pos(1)+width    pos(2)          pos(3);
        pos(1)          pos(2)          pos(3)+height;
        pos(1)          pos(2)+deep     pos(3)+height; 
        pos(1)+width    pos(2)+deep     pos(3)+height;
        pos(1)+width    pos(2)          pos(3)+height];
    
fac = [1 2 3 4; ...
       2 6 7 3; ...
       4 3 7 8; ...
       1 5 8 4; ...
       1 2 6 5; ...
       5 6 7 8];

 hRec = patch('Faces',fac,'Vertices',vert,'FaceColor',color,'Parent',parent,'EdgeAlpha',lines,'FaceAlpha',alpha);
 
 return
