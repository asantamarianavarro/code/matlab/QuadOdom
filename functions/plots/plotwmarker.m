function [hndl] = plotwmarker(x,y,linewidth,markersize,markertype,markerevery,color)

plot(x,y(:),color,'LineWidth',linewidth);
plot(x(1:markerevery:end),y(1:markerevery:end),strcat(markertype,color),'MarkerSize',markersize,'LineWidth',linewidth);
hndl = plot(x(1),y(1),strcat(markertype,'-',color),'MarkerSize',markersize);

return