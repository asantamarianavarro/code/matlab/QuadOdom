% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Plot estimation results with covariances
% 
%   [] = plot_estwcov(6,plotparams,robots)
% 
%   This functions plots the estimation results (state) with the
%   corresponding covariances (+- 3 sigma). it can also plot the estimation
%   errors with the covariances.
% 
%   - Inputs:
%       - plot_params:   Plot parameters.
%       - robots:   Robots structure with estimation and ground truth data.
% 
%   - Outputs:
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [] = plot_estwcov(fig,plotparams,robots)

% Find robot number and check there exist ground truth
gtruth_found = false;
robot_found = false;
for ii = 1:size(robots,2)
   if strcmp(robots(ii).name,plotparams.estwcov.robot_name)
       robnum = ii;
       robot_found = true;
   end
   if strcmp(robots(ii).name,'gtruth')
       gtruth_found = true;
   end
end

if ~gtruth_found
    error('Estimation errors plot: ground truth not found.');
end
if ~robot_found
    error('Estimation errors plot: specified Robot not found.');    
end

% Define variables
t_imu = robots(robnum).t; % Time vector
xstate = robots(robnum).state; % State vector
P = robots(robnum).P; % System covariance
diagP = zeros(size(P,1),size(P,3)); % Diagonal elements to plot
for ii = 1:size(P,3)
    diagP(:,ii)=diag(P(:,:,ii));
end
state_cov = diagP; % Covariances


if strfind(plotparams.estwcov.robot_name,'EKF')
    met = 'ekf';
elseif strfind(plotparams.estwcov.robot_name,'ESKF')
    met = 'eskf';
else
    error(strcat('Cannot plot data from robot: ',plotparams.estwcov.robot_name,'. Its name does not contain the filter type (ekf or eskf).'));
end
if strfind(plotparams.estwcov.robot_name,'G')
    frame = 'global';
elseif strfind(plotparams.estwcov.robot_name,'L')
    frame = 'local';
else
    error(strcat('Cannot plot data from robot: ',plotparams.estwcov.robot_name,'. Its name does not contain where the orientation error is expressed (global or local).'));
end

% Compute the error for each state term
gtstate = robots(1).state;

% Get Angles
e_hat = zeros(3,size(t_imu,2));
gte_hat = zeros(3,size(t_imu,2));
state_e_cov = zeros(18,size(t_imu,2));
for ii=1:size(t_imu,2)
    [e_hat(:,ii),J_q] = q2e(xstate(7:10,ii));
    gte_hat(:,ii) = q2e(gtstate(7:10,ii));
    if(strcmp(met,'ekf'))
            state_e_cov(1:6,ii) = state_cov(1:6,ii);
            state_e_cov(10:18,ii) = state_cov(11:19,ii);
            El = J_q*diag(state_cov(7:10,ii))*J_q';
            state_e_cov(7:9,ii) = diag(El);
            if(strcmp(frame,'global'))
                R = q2R(xstate(7:10,ii));
                Eg = R*El*R';
                state_e_cov(7:9,ii) = diag(Eg);
            end                
    end
end

ep = zeros(3,size(gtstate,2));
ev = zeros(3,size(gtstate,2));
er = zeros(3,size(gtstate,2));
if plotparams.estwcov.plot_errors
    for ii=1:size(t_imu,2)
        ep(:,ii) = gtstate(1:3,ii) - xstate(1:3,ii);
        ev(:,ii) = gtstate(4:6,ii) - xstate(4:6,ii);
        er(:,ii) = gte_hat(:,ii) - e_hat(:,ii);
    end
else
    ep = xstate(1:3,:);
    ev = xstate(4:6,:);
    er = e_hat;    
end

switch met
    case 'eskf'           
        plot_3axis_wcov(fig,'Position',plotparams.estwcov.plot_errors,t_imu,ep,state_cov(1:3,:),'$x$',[1 0 0],'$y$',[0 0.7 0.3],'$z$',[0 0 1],'$s$','$m$');    
        plot_3axis_wcov(fig+1,'Velocity',plotparams.estwcov.plot_errors,t_imu,ev,state_cov(4:6,:),'$v_x$',[1 0 0],'$v_y$',[0 0.7 0.3],'$v_z$',[0 0 1],'$s$','$m/s$'); 
        plot_3axis_wcov(fig+2,'Angles',plotparams.estwcov.plot_errors,t_imu,er,state_cov(7:9,:),'$\phi$',[1 0 0],'$\theta$',[0 0.7 0.3],'$\psi$',[0 0 1],'$s$','$rad$');
        plot_3axis_wcov(fig+3,'Acc.Bias',plotparams.estwcov.plot_errors,t_imu,xstate(11:13,:),state_cov(10:12,:),'$a_{bx}$',[1 0 0],'$a_{by}$',[0 0.7 0.3],'$a_{bz}$',[0 0 1],'$s$','$m/s^2$');
        plot_3axis_wcov(fig+4,'Ang.Vel.Bias',plotparams.estwcov.plot_errors,t_imu,xstate(14:16,:),state_cov(13:15,:),'$\omega_{bx}$',[1 0 0],'$\omega_{by}$',[0 0.7 0.3],'$\omega_{bz}$',[0 0 1],'$s$','$rad/s$');
    case 'ekf'    
        plot_3axis_wcov(fig,'Position',plotparams.estwcov.plot_errors,t_imu,ep,state_e_cov(1:3,:),'$x$',[1 0 0],'$y$',[0 0.7 0.3],'$z$',[0 0 1],'$s$','$m$');
        plot_3axis_wcov(fig+1,'Velocity',plotparams.estwcov.plot_errors,t_imu,ev,state_e_cov(4:6,:),'$v_x$',[1 0 0],'$v_y$',[0 0.7 0.3],'$v_z$',[0 0 1],'$s$','$m/s$'); 
        plot_3axis_wcov(fig+2,'Angles',plotparams.estwcov.plot_errors,t_imu,er,state_e_cov(7:9,:),'$\phi$',[1 0 0],'$\theta$',[0 0.7 0.3],'$\psi$',[0 0 1],'$s$','$rad$');
        plot_3axis_wcov(fig+3,'Acc.Bias',plotparams.estwcov.plot_errors,t_imu,xstate(10:12,:),state_e_cov(11:13,:),'$a_{bx}$',[1 0 0],'$a_{by}$',[0 0.7 0.3],'$a_{bz}$',[0 0 1],'$s$','$m/s^2$');
        plot_3axis_wcov(fig+4,'Ang.Vel.Bias',plotparams.estwcov.plot_errors,t_imu,xstate(14:16,:),state_e_cov(14:16,:),'$\omega_{bx}$',[1 0 0],'$\omega_{by}$',[0 0.7 0.3],'$\omega_{bz}$',[0 0 1],'$s$','$rad/s$');
end

return