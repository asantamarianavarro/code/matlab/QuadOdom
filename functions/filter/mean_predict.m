% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   MEAN STATE PREDICTION
%
%   [xstate_nom] = mean_predict(xstate,a_s,w_s,dt,qint_met)
%
%   Returns the prediction step for the nominal-state vector.
%   
%   Inputs:
%       - xstate:   Nominal-state vector at time k-1.
%       - a_s:      Acc. reading at time k.
%       - w_s:      3x2 matrix with Gyro readings (columns) at time k-1 and k.
%       - dt:       Time step between k-1 and k.
%       - qint_met: Quaternion integration method.
% 
%   Outputs:
%       - xstate_nom:   Nominal-state vector estimate at time k.
%
%   NOTE: In Error-State Kalman Filter (ESKF) the error state vector 
%   starts always being 0, thus this step state = Fx*state_old is not needed 
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [xstate_nom]=mean_predict(xstate,a_s,w_s,dt,qint_met,trunc_met)

% Propagate Bias and gravity (considering linear during integration) ___
p = xstate(1:3,1);
v = xstate(4:6,1);
ab = xstate(11:13,1);
wb = xstate(14:16,1);
g = xstate(17:19,1);

% Angular velocity in Inertial frame (= body Frame, w/o bias)
w = w_s-repmat(wb,1,2);

% Relative rotation due to last angular velocity
q_nom = qPredict(xstate(7:10,1),w,dt,qint_met);

% Acceleration in Inertial frame (w/o bias and g). This has to be coherent
% with Quaternionn integration method. The time step measure must be the same 
% at k-1 or k

switch lower(qint_met)
    case 'zerof'
        a = q2R(q_nom)*(a_s(:,2)-ab)+g;    
    case 'zerob'
        a = q2R(q_nom)*(a_s(:,1)-ab)+g;
    case 'first'
        a_avg = a_s(:,1) + 0.5*(a_s(:,2)-a_s(:,1));
        a = q2R(q_nom)*(a_avg-ab)+g;
end

if trunc_met == 1
    p_nom = p + v*dt; % Linear position in Inertial frame (integrating vel. and acc.)
    v_nom = v + a*dt; % Linear velocity in Inertial frame (integrating acc.)
elseif trunc_met == 2
    p_nom = p + v*dt + 0.5*a*dt^2; % Linear position in Inertial frame (integrating vel. and acc.)
    v_nom = v + a*dt; % Linear velocity in Inertial frame (integrating acc.)    
elseif trunc_met == 3
    p_nom = p + v*dt + 0.5*a*dt^2; % Linear position in Inertial frame (integrating vel. and acc.)
    v_nom = v + a*dt; % Linear velocity in Inertial frame (integrating acc.)        
else
    error('Mean predict: Wrong IMU truncation grade (it must be 1, 2 or 3).');
end

    xstate_nom = [p_nom;v_nom;q_nom;ab;wb;g]; % Nominal state
    
return