% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Nominal State vector update considering additive model.
%
%   [xstate] = ESKFupdate(xstate_old,dxstate,frame)
%
%   Returns current state vector from previous state vector and
%   the error state estimation obtained with the filter.
%
%   Inputs:
%       - xstate_old:   Last state vector (previous step).
%       - dxstate:      Error state estimated with the filter.
%       - frame:        Frame whether the orientation error is specified.
% 
%   Ouputs:
%       - xstate:       New state vector.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [xstate] = ESKFupdate(xstate_old,dxstate,frame)

qe = vec2q(dxstate(7:9));

xstate(1:6,1) = xstate_old(1:6,1) + dxstate(1:6,1);
xstate(11:19,1) = xstate_old(11:19,1) + dxstate(10:18,1);

switch lower(frame)
    case 'local'
        xstate(7:10,1) = qNorm(qProd(xstate_old(7:10,1),qe));
    case 'global'
        xstate(7:10,1) = qNorm(qProd(qe,xstate_old(7:10,1)));
end


return