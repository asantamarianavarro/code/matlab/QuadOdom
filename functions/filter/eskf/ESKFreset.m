% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Error State Kalman Filter Reset
%
%   [dxstate,P] = ESKFreset(dxstate_old,P_old,frame)
%
%   Computes the reset of the error state to start the next interation
%   and update the covariance matrix accordingly.
%   
%   Inputs:
%       - dxatate_old:  Error state vector at time k-1.
%       - P_old:        Covariance matrix at time k-1.
%       - frame:        Frame where the orientation error is represented.
% 
%   Outputs:
%       - dxstate:      Error state vector at time k.
%       - P:            Covariance matrix at time k.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [dxstate,P] = ESKFreset(dxstate_old,P_old,frame)

switch lower(frame)
    case 'local'
        R = vec2R(-dxstate_old(7:9)/2);
    case 'global'
        R = vec2R(dxstate_old(7:9)/2);
end

G = [eye(6)        zeros(6,3)  zeros(6,9);
    zeros(3,6)     R           zeros(3,9);
    zeros(9,6)     zeros(9,3)  eye(9)];

P = G*P_old*G';

dxstate(:,1) = zeros(18,1);

return
