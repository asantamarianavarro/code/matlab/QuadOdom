% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Pose sensor observation model and Jacobian matrix.
%
%   [h,H_xstate,H_dxstate] = pose_obs_model(xstate,frame,std_pose)
%
%   Returns the Observation Jacobian from current nominal state vector
%   
%   Inputs:
%       - state:        State vector.
%       - frame:        Frame where the orientation error is expressed.
%       - params:       Sensor parameters.
%       - std_pose:     Noise std dev.
%
%   Ouputs:
%       - h:         Sensor readings
%       - H_xstate:  Observation Jacobian w.r.t. nominal state (EKF)
%       - H_dxstate: Observation Jacobian w.r.t. error state (ESKF)
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [h,H_xstate,H_dxstate] = pose_obs_model(xstate,std_pose)

if nargin == 1
    std_pose=zeros(6,1);
end

%     p = xstate(1:3,end);
%     [theta,J_q] = q2e(xstate(7:10,end));
    p = xstate(1:3);
    [theta,J_q] = q2e(xstate(7:10));
  
    % Observation model  
    h = [p;theta] + randn(1,1).*std_pose;
    
    % Observation Jacobian 
    if nargout > 2
        H_dxstate = [eye(3,3)    zeros(3,15);
                     zeros(3,6)  eye(3,3) zeros(3,9)];
                 
        H_xstate = [eye(3,3)    zeros(3,16);
                    zeros(3,6)  J_q    zeros(3,9)];
    end

return
