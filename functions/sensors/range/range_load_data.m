% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Load Range data from .txt (real) or .mat (simulation) files.
%
%   [range_s,of_qual]=range_load_data(path)
%
%   Returns the sensor measurements from Range sensor.
%   
%   Inputs:
%       - path:    Path where the data is stored.
%
%   Ouputs:
%       - range_s:      Sensor measurement. (m) 
%       - time:         Time stamp of sensor readings starting at 0. (s)  
%       - range_params: Particular parameters of the sensor.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [range_s,time]=range_load_data(path)

% Add path
warning ('off','all');
rmpath(genpath('data'));
warning ('on','all');
addpath(path);

if strfind(path, 'simu')
    disp('-> Loading simulation Range data...')    
    
    file = [path 'range.mat'];
    load(file);
    
    % TIME: measures stamps (starting point at 0, measures in secs)
    time = range(:,1)';

    % RANGE
    range_s = range(:,2)';
        
else
    disp('-> Loading real Range data...')        

    % PX4 Optical Flow sensor output measured in Sensor frame 
    file='range.txt';
    if(exist(file, 'file')==2) 
        load(file);
        data = range(:,1:end)';
        
        % TIME: measures stamps (starting point at 0, measures in secs)
        time=data(3,:)'; 
        time=((time-time(1))*10^-9)';

        % RANGE
        range_s = data(4,:);
    end
end

    
return