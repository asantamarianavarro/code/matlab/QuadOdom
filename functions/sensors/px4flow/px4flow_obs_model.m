% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Optical FLow observation model and Jacobian matrix.
%
%   [h,H_xstate,H_dxstate] = px4flow_obs_model(xstate,frame,std_sim_of,w)
%
%   Returns the Observation Jacobian from current nominal state vector
%   
%   Inputs:
%       - state:        State vector.
%       - frame:        Frame where the orientation error is expressed.
%       - params:       Sensor parameters.
%       - w:            Angular velocity of the sensor.
%       - std_px4flow:  Noise std dev.
%
%   Ouputs:
%       - h:         Sensor readings
%       - H_xstate:  Observation Jacobian w.r.t. nominal state (EKF)
%       - H_dxstate: Observation Jacobian w.r.t. error state (ESKF)
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [h,H_xstate,H_dxstate] = px4flow_obs_model(xstate,frame,params,w,std_px4flow)

if nargin == 1
    frame='local';
    params.datatype = 'vxy';
    params.focal = 500;
    w = zeros(3,1);
    std_px4flow=zeros(3,1);
elseif nargin == 2
    params.datatype = 'vxy';
    params.focal = 500;
    w = zeros(3,1);
    std_px4flow=zeros(3,1);
elseif nargin == 3
    w = zeros(3,1);
    std_px4flow=zeros(3,1);    
elseif nargin == 4
    std_px4flow=zeros(3,1);        
end

p = xstate(1:3);
v = xstate(4:6);
[qc,QC_q] = q2qc(xstate(7:10));
[~, W_v, W_qc] = qRot(v,qc);
W_q =  W_qc * QC_q;
R_trans = q2R(qc);

Sxy = [eye(2,2) zeros(2,1)];
Sz = [0 0 1]; 

if strcmp(params.datatype,'flow2d')
    
    % Force z always positive
    if (p(3) <= 0.0)
        p(3) = 1e-3;
    end
    
    % Observation model
    f = params.focal;
    wyx = [-w(2,1);w(1,1)];
    z = Sz*p;
    
    h = [Sz*p;-(f*Sxy*R_trans*v)/z + f*wyx] + randn(3,1).*std_px4flow(1:3);
        
    % Observation Jacobian 
    if nargout > 2

        Hz = f*Sxy*R_trans*v/(z^2); % Avoided due to denominator
%         Hz = zeros(2,1);
        Hv = -(f*Sxy*R_trans)/z;
        
        % Jacobian r.t. Error__________________________        
        switch lower(frame)
            case 'local'
                term_rot = vec2skew(R_trans*v);
            case 'global'
                term_rot = R_trans * vec2skew(v);
        end         
        Hr = -(f*Sxy*term_rot)/z; 

        H_dxstate = [ Sz              zeros(1,3)  zeros(1,3)  zeros(1,9);
                     [zeros(2,2) Hz]  Hv          Hr          zeros(2,9)];
    
        % Jacobian r.t. State__________________________
        Hr = -(f*Sxy*W_q)/z;       
        H_xstate = [Sz              zeros(1,3)  zeros(1,4)  zeros(1,9);
                    [zeros(2,2) Hz] Hv          Hr          zeros(2,9)];       
    end    
    
    
elseif strcmp(params.datatype,'vxy')

    % Observation model
    h = [Sz*p;Sxy*R_trans*v] + randn(3,1).*[std_px4flow(1);std_px4flow(4:5)];

    % Observation Jacobian 
    if nargout > 2

        switch lower(frame)
            case 'local'
                term_rot = Sxy*vec2skew(R_trans*v);
            case 'global'
                term_rot = Sxy*R_trans*vec2skew(v);        
        end    

        % Jacobian r.t. Error__________________________
        H_dxstate = [Sz          zeros(1,3)   zeros(1,3)  zeros(1,9);
                     zeros(2,3)  Sxy*R_trans  term_rot    zeros(2,9)];
    
        % Jacobian r.t. State__________________________  
        H_xstate = [Sz         zeros(1,3)  zeros(1,4)  zeros(1,9);
                   zeros(2,3)  Sxy*W_v     Sxy*W_q     zeros(2,9)];       
    end
else
  error('PX4Flow parameters incorrect. Datatype not properly defined.');
end
    

return

%% Check with symbolic library (run section)

syms vx vy vz px py pz qw qx qy qz real

q=[qw;qx;qy;qz]
p=[px;py;pz]
v=[vx;vy;vz]

[dq,DQ_dtheta]=vec2q([0;0;0]);

[qt,Qt_q,Qt_dq] = qProd(q,dq)

xstate = [p;v;qt]

% Our jacobian
[ht,Ht_x,Ht_dx]=px4flow_obs_model([p;v;q])

%Simbolic Jacobian
Ht_xstate = simplify(jacobian(ht,xstate))

Qt_dtheta = Qt_dq * DQ_dtheta
Vof_qt = Ht_xstate(2:3,7:10);
Vof_dtheta = Vof_qt * Qt_dtheta;

Ht_dx2 = [Ht_xstate(:,1:6) [zeros(1,3);Vof_dtheta] zeros(3,9)];

%Difference
H_err = simplify(Ht_dx - Ht_dx2)

