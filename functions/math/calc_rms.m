% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Position RMSE and STD of between two trajectories
% 
%   [error_rms,error_std,error] = calc_rms(gtruth,tgt,est,test)
% 
%   Computes the position error, RMSE and STD betweeen a ground truth and
%   an estimated trajectory.
% 
%   - Inputs:
%       - gtruth:   Ground truth state. [p v q w]. Hamilton quaternion [qw;qx;qy;qz]
%       - tgt:      Ground truth time vector.
%       - est:      Estimation state. [p v q w]. Hamilton quaternion [qw;qx;qy;qz]
%       - test:     Estimation time vector.
% 
%   - Outputs:
%       - error_rms:    Position Root Mean Squared Error.
%       - error_std:    Position Error STD.
%       - error:        Position Error.
%   
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [error_rms,error_std,error] = calc_rms(gtruth,tgt,est,test)

[m, n] = size(est);
error  = zeros(m, n);

for i = 1:m
    error(i,:) = interp1(tgt, gtruth(i,:), test, 'spline') - est(i,:);
end

error_std = std(error, 1, 2);
error_rms = sqrt(mean(error.^2,2));

end
