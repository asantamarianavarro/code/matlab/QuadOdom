% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   SYNCHRONIZE ROBOTS
% 
%   This functions returns the synchronized estimation w.r.t. the ground truth
%   considering their time stamps. The output robot has the size as the
%   ground truth to facilitate plots.
% 
%   - Inputs:
%       - Rob:      Robot to be synchronized.
%       - GTRob:    Ground Truth Robot. 
%   - Outputs:
%       - Rob_sync: Synchronized Robot.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [Rob_sync] = robotsync(Rob,GTRob)

for ii = 1:size(GTRob.t,2)

    % Find index of closest value     
    tmp = abs(Rob.t - GTRob.t(1,ii));
    [~,idx] = min(tmp);
    tstamp(:,ii) = Rob.t(1,idx);
    state(:,ii) = Rob.state(:,idx); 
    if ~isempty(Rob.P)
        P(:,:,ii) = Rob.P(:,:,idx);
    else
        P = [];
    end
    
end

Rob_sync = newrobot(Rob.name,Rob.color,tstamp,state,P,Rob.size_m2m,Rob.frame_len,Rob.obs_rate);

% Set GTRob to initial position and orientation
if strcmp(Rob.name,'gtruth') && norm(Rob.state(1:3,1))~=0
   for ii = 1:size(Rob_sync.state,2)
      Rob_sync.state(1:3,ii) = Rob_sync.state(1:3,ii) - Rob.state(1:3,1);
      Rob_sync.state(7:10,ii) = qProd(Rob.state(7:10,ii),Rob_sync.state(7:10,1));
   end    
end

return