% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   RANGE from ROSBAG
% 
%   [tstamp,range_s] = range_from_rosbag(rosbag_file,topic_name)
% 
%   This script retrieves Range messages from a robsbag containing a 
%   sensor_msgs/Range topic.
% 
%   - Inputs:
%       - bagfile:      Rosbag file (see 'help rosbag').
%       - topic_name:   Topic name.
% 
%   - Outputs:
%       - tstamp:       Messages time stamp. Initialized w.r.t. bag time.
%       - range_s:      Range readings. [r]
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [tstamp,range_s] = range_from_rosbag(bagfile,topic_name)

disp('-> Loading RANGE data...')

% Get topic
Data = select(bagfile,'Topic',topic_name);

% Get messages
Msgs = readMessages(Data);

% Initialize structures
numMsgs = size(Msgs,1);
tstamp = zeros(1,numMsgs);
range_s = zeros(1,numMsgs);

% get initial BAG time
tini = double(bagfile.StartTime);

% Mount output structures
for ii = 1:numMsgs

   tstamp(1,ii) = double(Msgs{ii}.Header.Stamp.Sec) ...
                  + double(Msgs{ii}.Header.Stamp.Nsec)*1e-9 ...
                  - tini;
              
   range_s(1,ii) = [double(Msgs{ii}.Range_)];
    
end

return