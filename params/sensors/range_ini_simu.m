% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Range Initial Simulation Values
% 
%   Initializes all variables needed by the filter for the SIMULATED Range sensor.
%
%   [params] = range_ini_simu()
%
%   Ouputs:
%       - params: Sensor parameters
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [params] = range_ini_simu()

% Parameters
params.bounds.lower = 0.3; % Sensor minimum range (m)  
params.bounds.upper = 4.0; % Sensor maximum range (m)
params.std.insidebouds = 0.05;  % Std dev when inside bounds
params.std.outsidebouds = 10; % Std dev when outside bounds
params.model = 'time-of-flight'; % Model type: 'sonar' or 'time-of-flight'

return